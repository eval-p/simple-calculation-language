package org.scl.syntax;

import org.junit.runners.Parameterized.Parameters;

import java.util.Collection;

import static java.util.Arrays.asList;

/**
 * @author Evgeny Pavlovsky
 */
public class RelationalExpressionSyntaxTest extends SyntaxTest {


  @Parameters(name = "file: {0}")
  public static Collection<Object[]> data() {
    return asList(new Object[][]{
      {"missing_operator.scl",
        msg("missing {'==', '!=', '>', '<', '>=', '<='}", "line: 1, position: 6")},

      {"missing_left_operand.scl",
        msg("extraneous input '=='", "line: 1, position: 5")},

      {"missing_right_operand.scl",
        msg("mismatched input ')'", "line: 1, position: 8")},

      {"double_operator_usage.scl",
        msg("mismatched input '>'", "line: 1, position: 14")}
    });
  }


  @Override
  String getPathSuffix() {
    return "expression/relational/";
  }
}
