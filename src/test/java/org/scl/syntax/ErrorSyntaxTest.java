package org.scl.syntax;

import org.junit.runners.Parameterized;

import java.util.Collection;

import static java.util.Arrays.asList;

/**
 * @author Evgeny Pavlovsky
 */
public class ErrorSyntaxTest extends SyntaxTest {

  @Parameterized.Parameters(name = "file: {0}")
  public static Collection<Object[]> data() {
    return asList(new Object[][]{
      {"missing_semicolon.scl",
        msg("missing ';' at '<EOF>' (line: 3, position: 0)")},

      {"missing_message.scl",
        msg("missing StringLiteral at ';' (line: 3, position: 6)")},

      {"missing_message_body.scl",
        msg("Error message cannot be null or blank.")},

      {"missing_right_quote.scl",
        msg("mismatched input ''' expecting StringLiteral (line: 6, position: 6)")},

      {"missing_left_quote.scl",
        msg("mismatched input 'This' expecting StringLiteral (line: 2, position: 6)")}
    });
  }

  @Override
  String getPathSuffix() {
    return "error/";
  }
}
