package org.scl.syntax;

import org.junit.runners.Parameterized.Parameters;

import java.util.Collection;

import static java.util.Arrays.asList;

/**
 * @author Evgeny Pavlovsky
 */
public class VariableSyntaxTest extends SyntaxTest {

  @Parameters(name = "file: {0}")
  public static Collection<Object[]> data() {
    return asList(new Object[][]{
      {"parameter_keyword_1.scl",
        msg("mismatched input ':=' expecting Identifier (line: 1, position: 10)")},

      {"parameter_keyword_2.scl",
        msg("mismatched input 'parameter' expecting Identifier (line: 1, position: 10)")},

      {"result_keyword_1.scl",
        msg("extraneous input ':='", "line: 1, position: 7")},

      {"result_keyword_2.scl",
        msg("mismatched input 'result'", "line: 1, position: 7")},

      {"not_allowed_symbol_1.scl",
        msg("token recognition error at: '@' (line: 1, position: 2)")},

      {"not_allowed_symbol_2.scl",
        msg("token recognition error at: '$' (line: 1, position: 2)")},

      {"starting_from_digit.scl",
        msg("extraneous input '1'", "line: 1, position: 0")},

      {"starting_from_underscore.scl",
        msg("token recognition error at: '_' (line: 1, position: 0)")}
    });
  }

  @Override
  String getPathSuffix() {
    return "variable/";
  }
}
