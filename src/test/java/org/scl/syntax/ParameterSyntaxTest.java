package org.scl.syntax;

import org.junit.runners.Parameterized.Parameters;

import java.util.Collection;

import static java.util.Arrays.asList;

/**
 * @author Evgeny Pavlovsky
 */
public class ParameterSyntaxTest extends SyntaxTest {

  @Parameters(name = "file: {0}")
  public static Collection<Object[]> data() {
    return asList(new Object[][]{
      {"missing_name.scl",
        msg("missing Identifier at ';' (line: 2, position: 10)")},

      {"missing_semicolon.scl",
        msg("missing ';' at 'result' (line: 3, position: 0)")},

      {"not_first_instruction_1.scl",
        msg("mismatched input 'parameter' expecting <EOF> (line: 3, position: 0)")},

      {"not_first_instruction_2.scl",
        msg("mismatched input 'parameter' expecting <EOF> (line: 3, position: 0)")}
    });
  }

  @Override
  String getPathSuffix() {
    return "parameter/";
  }
}
