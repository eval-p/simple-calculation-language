package org.scl.syntax;

import org.junit.runners.Parameterized.Parameters;

import java.util.Collection;

import static java.util.Arrays.asList;

/**
 * @author Evgeny Pavlovsky
 */
public class ArithmeticExpressionSyntaxTest extends SyntaxTest {


  @Parameters(name = "file: {0}")
  public static Collection<Object[]> data() {
    return asList(new Object[][]{
      {"unused_expression.scl",
        msg("mismatched input '+' expecting ':=' (line: 3, position: 1)")},

      {"unused_operator.scl",
        msg("mismatched input '+' expecting <EOF> (line: 3, position: 0)")},

      {"missing_right_operand.scl",
        msg("mismatched input ';'", "line: 1, position: 10")},

      {"missing_left_operand.scl",
        msg("extraneous input '*'", "line: 1, position: 6")},

      {"missing_middle_operand.scl",
        msg("extraneous input '/'", "line: 1, position: 8")},

      {"missing_two_operands.scl",
        msg("mismatched input '/'", "line: 1, position: 5")},

      {"missing_three_operands.scl",
        msg("mismatched input '*'", "line: 1, position: 5")},

      {"missing_left_parenthesis.scl",
        msg("mismatched input ')'", "line: 1, position: 24")},

      {"missing_right_parenthesis.scl",
        msg("missing ')' at ';'", "line: 1, position: 27")}
    });
  }

  @Override
  String getPathSuffix() {
    return "expression/arithmetic/";
  }
}
