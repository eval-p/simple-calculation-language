package org.scl.syntax;

import org.junit.runners.Parameterized;

import java.util.Collection;

import static java.util.Arrays.asList;

/**
 * @author Evgeny Pavlovsky
 */
public class WhileStatementSyntaxTest extends SyntaxTest {


  @Parameterized.Parameters(name = "file: {0}")
  public static Collection<Object[]> data() {
    return asList(new Object[][]{
      {"semicolon_present.scl",
        msg("mismatched input ';'", "line: 5, position: ")},

      {"empty_body.scl",
        msg("extraneous input '}'", "line: 4, position: 0")},

      {"missing_condition.scl",
        msg("mismatched input ')'", "line: 3, position: 7")},

      {"missing_body.scl",
        msg("mismatched input '<EOF>'", "line: 4, position: 0")},

      {"missing_opening_parenthesis.scl",
        msg("missing '('", "line: 3, position: 6")},

      {"missing_closing_parenthesis.scl",
        msg("missing ')'", "line: 3, position: 14")},

      {"incorrect_condition_1.scl",
        msg("mismatched input ')'", "line: 3, position: 8")},

      {"incorrect_condition_2.scl",
        msg("mismatched input ')'", "line: 3, position: 9")},

      {"incorrect_condition_3.scl",
        msg("mismatched input ':='", "line: 3, position: 8")},

      {"missing_opening_brace.scl",
        msg("missing '{' at 'a'", "line: 4, position: 2")},

      {"missing_closing_brace.scl",
        msg("extraneous input '<EOF>'", "line: 8, position: 0")},

    });
  }

  @Override
  String getPathSuffix() {
    return "while/";
  }
}
