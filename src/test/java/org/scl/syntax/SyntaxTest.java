package org.scl.syntax;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.scl.Result;

import static org.assertj.core.api.Assertions.assertThat;
import static org.scl.Scl.interpretFromFile;
import static org.scl.TestUtil.getAbsoluteFilename;

/**
 * @author Evgeny Pavlovsky
 */
@SuppressWarnings("WeakerAccess")
@RunWith(Parameterized.class)
public abstract class SyntaxTest {

  @Parameter
  public String filename;

  @Parameter(1)
  public String[] expectedMessages;

  @Test
  public void syntaxIsInvalid() {
    final Result result = execute();
    assertThat(result.isSuccessful()).isFalse();
    assertThat(result.getValue()).isNull();
    assertThat(result.getErrorMessage()).contains(expectedMessages);
  }

  String getPathSuffix() {
    return "";
  }

  static String[] msg(String... msg) {
    return msg;
  }

  private Result execute() {
    final String fullName = getAbsoluteFilename("/org/scl/syntax/" + getPathSuffix() + filename);
    return interpretFromFile(fullName);
  }

}
