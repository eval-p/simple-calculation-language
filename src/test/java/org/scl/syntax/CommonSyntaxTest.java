package org.scl.syntax;

import org.junit.runners.Parameterized.Parameters;

import java.util.Collection;

import static java.util.Arrays.asList;

/**
 * @author Evgeny Pavlovsky
 */
public class CommonSyntaxTest extends SyntaxTest {

  @Parameters(name = "file: {0}")
  public static Collection<Object[]> data() {
    return asList(new Object[][]{
      {"empty.scl",
        msg("mismatched input '<EOF>'", "line: 1, position: 0")},

      {"only_comments.scl",
        msg("mismatched input '<EOF>'", "line: 5, position: 0")},

      {"invalid_token.scl",
        msg("mismatched input 'result' expecting ':=' (line: 3, position: 0)")}
    });
  }
}
