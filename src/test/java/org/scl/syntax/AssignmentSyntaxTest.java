package org.scl.syntax;

import org.junit.runners.Parameterized.Parameters;

import java.util.Collection;

import static java.util.Arrays.asList;

/**
 * @author Evgeny Pavlovsky
 */
public class AssignmentSyntaxTest extends SyntaxTest {

  @Parameters(name = "file: {0}")
  public static Collection<Object[]> data() {
    return asList(new Object[][]{
      {"missing_semicolon.scl",
        msg("missing ';' at 'result' (line: 3, position: 0)")},

      {"missing_operator.scl",
        msg("missing ':=' at '10' (line: 1, position: 3)")},

      {"missing_value.scl",
        msg("mismatched input ';'", "line: 1, position: 4")},

      {"missing_operator_and_value.scl",
        msg("mismatched input ';' expecting ':=' (line: 1, position: 1)")},

      {"repeating_operator.scl",
        msg("extraneous input ':='", "line: 1, position: 6")}
    });
  }

  @Override
  String getPathSuffix() {
    return "assignment/";
  }
}
