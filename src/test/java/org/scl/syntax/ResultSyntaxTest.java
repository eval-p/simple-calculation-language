package org.scl.syntax;

import org.junit.runners.Parameterized.Parameters;

import java.util.Collection;

import static java.util.Arrays.asList;

/**
 * @author Evgeny Pavlovsky
 */
public class ResultSyntaxTest extends SyntaxTest {

  @Parameters(name = "file: {0}")
  public static Collection<Object[]> data() {
    return asList(new Object[][]{
      {"missing_semicolon.scl",
        msg("missing ';' at '<EOF>' (line: 3, position: 0)")},

      {"missing_value.scl",
        msg("mismatched input ';'", "line: 1, position: 7")}
    });
  }

  @Override
  String getPathSuffix() {
    return "result/";
  }
}
