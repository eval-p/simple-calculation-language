package org.scl.result;

import org.junit.Test;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.scl.Result;

import java.util.Collection;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Evgeny Pavlovsky
 */
@SuppressWarnings("WeakerAccess")
public class ErrorResultTest extends ResultTest {

  @Parameter(1)
  public String errorMessage;

  @Parameters(name = "file: {0}")
  public static Collection<Object[]> data() {
    return asList(new Object[][]{
      {"parameters.scl", "Invalid number of arguments. Expected: 2, found: 1.", args(8)},
      {"parameters.scl", "Invalid number of arguments. Expected: 2, found: 3.", args(34, 1, 8)},
      {"parameters.scl", "Invalid number of arguments. Expected: 2, found: 0.", null},
      {"no_result_statement.scl", "Script execution finished, but no result was returned.", null},
      {"remainder_of_the_division.scl", "Attempt to divide by zero.", args(3, 0)},
      {"exponentiation.scl", "Parameter b must be greater or equal to 0.", args(3, -2)},
      {"factorial.scl", "Attempt to calculate factorial for negative number.", args(-3)}
    });
  }

  @Test
  public void resultIsNotSuccessful() {
    final Result result = execute();
    assertThat(result).isNotNull();
    assertThat(result.isSuccessful()).isFalse();
    assertThat(result.getErrorMessage()).isEqualTo(errorMessage);
    assertThat(result.getValue()).isNull();
  }
}
