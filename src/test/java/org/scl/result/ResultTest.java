package org.scl.result;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.scl.Result;

import static org.scl.Scl.interpretFromFile;
import static org.scl.TestUtil.getAbsoluteFilename;

/**
 * @author Evgeny Pavlovsky
 */
@RunWith(Parameterized.class)
public abstract class ResultTest {

  @Parameter
  public String filename;
  @Parameter(2)
  public Integer[] arguments;

  Result execute() {
    final String fullName = getAbsoluteFilename("/org/scl/result/" + filename);
    return interpretFromFile(fullName, arguments);
  }

  static Integer[] args(Integer... arguments) {
    return arguments;
  }
}
