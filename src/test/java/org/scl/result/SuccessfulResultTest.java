package org.scl.result;

import org.assertj.core.api.SoftAssertions;
import org.junit.Test;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.scl.Result;

import java.util.Collection;

import static java.util.Arrays.asList;

/**
 * @author Evgeny Pavlovsky
 */
@SuppressWarnings("WeakerAccess")
public class SuccessfulResultTest extends ResultTest {

  @Parameter(1)
  public Integer resultValue;

  @Parameters(name = "file: {0}")
  public static Collection<Object[]> data() {
    return asList(new Object[][]{
      {"arithmetic_expressions.scl", 11111, null},

      {"parameters.scl", 35, args(3, 5)},

      {"simple_if_statement.scl", 1, args(10, 5)},
      {"simple_if_statement.scl", -1, args(2, 3)},
      {"simple_if_statement.scl", 0, args(1, 1)},

      {"point_placement.scl", 0, args(10, 20, 3)},
      {"point_placement.scl", 1, args(100, 200, 150)},
      {"point_placement.scl", 2, args(-10, 0, 10)},

      {"exponentiation.scl", 64, args(2, 6)},
      {"exponentiation.scl", -3125, args(-5, 5)},
      {"exponentiation.scl", 1, args(9, 0)},

      {"greatest_common_divisor.scl", 27, args(81, 54)},
      {"greatest_common_divisor.scl", 27, args(-81, -54)},
      {"greatest_common_divisor.scl", 30, args(450, -390)},

      {"remainder_of_the_division.scl", 5, args(54, 7)},
      {"remainder_of_the_division.scl", 0, args(0, 3)},
      {"remainder_of_the_division.scl", -1, args(-10, 3)},

      {"factorial.scl", 720, args(6)},
      {"factorial.scl", 1, args(0)},

      {"absolute_value.scl", 5, args(5)},
      {"absolute_value.scl", 5, args(-5)},
      {"absolute_value.scl", 0, args(0)},

      {"max_value.scl", 10, args(4, 10)},
      {"max_value.scl", 0, args(0, -10)},

      {"min_value.scl", 15, args(15, 96)},
      {"min_value.scl", -8, args(-1, -8)},

      {"fibonacci_number.scl", 0, args(0)},
      {"fibonacci_number.scl", -1, args(-1)},
      {"fibonacci_number.scl", 1, args(2)},
      {"fibonacci_number.scl", 34, args(9)},
      {"fibonacci_number.scl", -13, args(-7)},
    });
  }

  @Test
  public void resultIsSuccessful() {
    final Result result = execute();
    final SoftAssertions softly = new SoftAssertions();
    softly.assertThat(result).isNotNull();
    softly.assertThat(result.isSuccessful()).isTrue();
    softly.assertThat(result.getErrorMessage()).isNull();
    softly.assertThat(result.getValue()).isEqualTo(resultValue);
    softly.assertAll();
  }
}
