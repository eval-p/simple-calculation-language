package org.scl.interpreter.expression;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.scl.interpreter.error.SclTranslationException;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Evgeny Pavlovsky
 */
public class RelationalExpressionBuilderTest {
  @Rule
  public ExpectedException exception = ExpectedException.none();

  private RelationalExpressionBuilder builder = new RelationalExpressionBuilder();

  @Test
  public void buildsStatement() {
    final ArithmeticExpression left = ExpressionDataHelper.arithmeticExpression();
    final ArithmeticExpression right = ExpressionDataHelper.arithmeticExpression();
    final RelationalExpression expression = builder.leftOperand(left)
                                                   .rightOperand(right)
                                                   .operator(">")
                                                   .build();
    assertThat(expression).isNotNull();
    assertThat(expression.getOperator()).isEqualTo(RelationalOperator.GREATER);
    assertThat(expression.getLeftOperand()).isSameAs(left);
    assertThat(expression.getRightOperand()).isSameAs(right);
  }

  @Test
  public void buildsNewInstanceEachTime() {
    final ArithmeticExpression expression = ExpressionDataHelper.arithmeticExpression();
    final RelationalExpression e1 = builder.leftOperand(expression).rightOperand(expression).operator(">").build();
    final RelationalExpression e2 = builder.leftOperand(expression).rightOperand(expression).operator(">").build();
    assertThat(e1).isNotSameAs(e2);
  }

  @Test
  public void operatorCannotBeNull() {
    exception.expect(SclTranslationException.class);
    exception.expectMessage("Operator cannot be null.");
    final ArithmeticExpression arithmeticExpression = ExpressionDataHelper.arithmeticExpression();
    builder.leftOperand(arithmeticExpression)
           .rightOperand(arithmeticExpression)
           .build();
  }

  @Test
  public void leftOperandCannotBeNull() {
    exception.expect(SclTranslationException.class);
    exception.expectMessage("Left operand cannot be null.");
    final ArithmeticExpression arithmeticExpression = ExpressionDataHelper.arithmeticExpression();
    builder.operator(">")
           .rightOperand(arithmeticExpression)
           .build();
  }

  @Test
  public void rightOperandCannotBeNull() {
    exception.expect(SclTranslationException.class);
    exception.expectMessage("Right operand cannot be null.");
    final ArithmeticExpression arithmeticExpression = ExpressionDataHelper.arithmeticExpression();
    builder.operator(">")
           .leftOperand(arithmeticExpression)
           .build();
  }
}
