package org.scl.interpreter.expression;

import org.junit.Test;
import org.scl.interpreter.ExecutionContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.scl.interpreter.expression.ArithmeticOperator.ADD;
import static org.scl.interpreter.expression.ArithmeticOperator.DIV;
import static org.scl.interpreter.expression.ArithmeticOperator.MUL;
import static org.scl.interpreter.expression.ArithmeticOperator.SUB;

/**
 * @author Evgeny Pavlovsky
 */
public class ArithmeticExpressionTest {
  private ExecutionContext context = new ExecutionContext();

  @Test
  public void singleNumber() {
    Integer result = ArithmeticExpression.builder()
                                         .number(1)
                                         .build()
                                         .execute(context);
    assertThat(result).isEqualTo(1);
  }

  @Test
  public void simpleNegativeNumber() {
    final Integer result = ArithmeticExpression.builder()
                                                .neg()
                                                .number(1)
                                                .build()
                                                .execute(context);
    assertThat(result).isEqualTo(-1);
  }

  @Test
  public void simpleExpressionWithAdd() {
    Integer result = ArithmeticExpression.builder()
                                         .number(2)
                                         .binaryOperator("+")
                                         .number(3)
                                         .build()
                                         .execute(context);
    assertThat(result).isEqualTo(5);
  }

  @Test
  public void simpleExpressionWithSub() {
    Integer result = ArithmeticExpression.builder()
                                         .number(13)
                                         .binaryOperator("-")
                                         .number(7)
                                         .build()
                                         .execute(context);
    assertThat(result).isEqualTo(6);
  }

  @Test
  public void simpleExpressionWithMul() {
    Integer result = ArithmeticExpression.builder()
                                         .number(9)
                                         .binaryOperator("*")
                                         .number(6)
                                         .build()
                                         .execute(context);
    assertThat(result).isEqualTo(54);
  }

  @Test
  public void simpleExpressionWithDiv() {
    Integer result = ArithmeticExpression.builder()
                                         .number(39)
                                         .binaryOperator("/")
                                         .number(3)
                                         .build()
                                         .execute(context);
    assertThat(result).isEqualTo(13);
  }

  @Test
  public void simpleExpressionWithNeg() {
    Integer result = ArithmeticExpression.builder()
                                         .neg()
                                         .number(10)
                                         .div()
                                         .number(2)
                                         .build()
                                         .execute(context);
    assertThat(result).isEqualTo(-5);
  }

  @Test
  public void simpleExpressionWithVariable() {
    context.assignVariable("a", 16);
    final Integer result = ArithmeticExpression.builder()
                                               .number(25)
                                               .binaryOperator("-")
                                               .variable("a")
                                               .build()
                                               .execute(context);
    assertThat(result).isEqualTo(9);
  }

  @Test
  public void complexExpression() {
    context.assignVariable("a", 7);
    context.assignVariable("b", 1);

    Integer result = ArithmeticExpression.builder()    //  24/2 + a*3 - b = 32
                                         .number(24)
                                         .operator(DIV)
                                         .number(2)
                                         .operator(ADD)
                                         .variable("a")
                                         .operator(MUL)
                                         .number(3)
                                         .operator(SUB)
                                         .variable("b")
                                         .build()
                                         .execute(context);
    assertThat(result).isEqualTo(32);
  }

  @Test
  public void executionOfOperatorsWithSamePriority() {
    Integer result = ArithmeticExpression.builder()     //  1 - 2*3 + 4 = -1
                                         .number(1)
                                         .sub()
                                         .number(2)
                                         .mul()
                                         .number(3)
                                         .add()
                                         .number(4)
                                         .build()
                                         .execute(context);
    assertThat(result).isEqualTo(-1);
  }

  @Test
  public void expressionWithBrackets() {
    Integer result = ArithmeticExpression.builder()      //  (1+2) * 3 - (5-1) = 5
                                         .openingBracket()
                                         .number(1)
                                         .add()
                                         .number(2)
                                         .closingBracket()
                                         .mul()
                                         .number(3)
                                         .sub()
                                         .openingBracket()
                                         .number(5)
                                         .sub()
                                         .number(1)
                                         .closingBracket()
                                         .build()
                                         .execute(context);
    assertThat(result).isEqualTo(5);
  }

  @Test
  public void expressionWithNestedBrackets() {
    Integer result = ArithmeticExpression.builder()       //(3 - (2+1) * 4) * 5 = -45
                                         .openingBracket()
                                         .number(3)
                                         .sub()
                                         .openingBracket()
                                         .number(2)
                                         .add()
                                         .number(1)
                                         .closingBracket()
                                         .mul()
                                         .number(4)
                                         .closingBracket()
                                         .mul()
                                         .number(5)
                                         .build()
                                         .execute(context);
    assertThat(result).isEqualTo(-45);
  }
}
