package org.scl.interpreter.expression;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.scl.interpreter.error.SclTranslationException;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Evgeny Pavlovsky
 */
public class ArithmeticExpressionBuilderTest {
  @Rule
  public ExpectedException exception = ExpectedException.none();

  private ArithmeticExpressionBuilder builder = new ArithmeticExpressionBuilder();

  @Test
  public void buildsStatement() {
    final ArithmeticExpression expression = builder.number(1).build();
    assertThat(expression).isNotNull();
    assertThat(expression.getElements()).hasSize(1);
  }

  @Test
  public void buildsNewInstanceEachTime() {
    final ArithmeticExpression e1 = builder.number(1).build();
    final ArithmeticExpression e2 = builder.number(1).build();
    assertThat(e1).isNotSameAs(e2);
  }

  @Test
  public void expressionMustNotBeEmpty() {
    exception.expect(SclTranslationException.class);
    exception.expectMessage("Expression cannot be empty.");
    builder.build();
  }

  @Test
  public void noOperands() {
    exception.expect(SclTranslationException.class);
    exception.expectMessage("Incorrect expression: '+'.");
    builder.add().build();
  }

  @Test
  public void notEnoughOperands() {
    exception.expect(SclTranslationException.class);
    exception.expectMessage("Incorrect expression: '1/'.");
    builder.number(1).div().build();
  }

  @Test
  public void missingClosingBracketInSimpleCase() {
    exception.expect(SclTranslationException.class);
    exception.expectMessage("Missing closing bracket in arithmetic expression '(1'.");

    ArithmeticExpression.builder()
                        .openingBracket()
                        .number(1)
                        .build();
  }

  @Test
  public void missingClosingBracket() {
    exception.expect(SclTranslationException.class);
    exception.expectMessage("Missing closing bracket in arithmetic expression '5-((1+2)*3-4'.");

    ArithmeticExpression.builder()   // 5 - ((1+2)*3 - 4
                        .number(5)
                        .sub()
                        .openingBracket()
                        .openingBracket()
                        .number(1)
                        .add()
                        .number(2)
                        .closingBracket()
                        .mul()
                        .number(3)
                        .sub()
                        .number(4)
                        .build();
  }

  @Test
  public void missingOpeningBracketInSimpleCase() {
    exception.expect(SclTranslationException.class);
    exception.expectMessage("Missing opening bracket in arithmetic expression '1)'.");

    ArithmeticExpression.builder()
                        .number(1)
                        .closingBracket()
                        .build();
  }

  @Test
  public void missingOpeningBracket() {
    exception.expect(SclTranslationException.class);
    exception.expectMessage("Missing opening bracket in arithmetic expression '5-(1+2)*3-4)'.");

    ArithmeticExpression.builder()   // 5 - (1+2)*3 - 4)
                        .number(5)
                        .sub()
                        .openingBracket()
                        .number(1)
                        .add()
                        .number(2)
                        .closingBracket()
                        .mul()
                        .number(3)
                        .sub()
                        .number(4)
                        .closingBracket()
                        .build();
  }

  @Test
  public void variableNameCannotBeNull() {
    exception.expect(SclTranslationException.class);
    exception.expectMessage("Variable name cannot be null or blank.");
    ArithmeticExpression.builder().variable(null);
  }

  @Test
  public void variableNameCannotBeBlank() {
    exception.expect(SclTranslationException.class);
    exception.expectMessage("Variable name cannot be null or blank.");
    ArithmeticExpression.builder().variable(" ");
  }
}
