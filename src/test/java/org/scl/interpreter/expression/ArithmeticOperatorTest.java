package org.scl.interpreter.expression;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.scl.interpreter.expression.ArithmeticOperator.ADD;
import static org.scl.interpreter.expression.ArithmeticOperator.DIV;
import static org.scl.interpreter.expression.ArithmeticOperator.MUL;
import static org.scl.interpreter.expression.ArithmeticOperator.NEG;
import static org.scl.interpreter.expression.ArithmeticOperator.SUB;

/**
 * @author Evgeny Pavlovsky
 */
public class ArithmeticOperatorTest {


  @Test
  public void additionOperator() {
    assertThat(ADD.operate(12, 8)).isEqualTo(20);
  }

  @Test
  public void subtractionOperator() {
    assertThat(SUB.operate(52, 67)).isEqualTo(-15);
  }

  @Test
  public void multiplicationOperator() {
    assertThat(MUL.operate(18, 4)).isEqualTo(72);
  }

  @Test
  public void divisionOperator() {
    assertThat(DIV.operate(33, -11)).isEqualTo(-3);
  }

  @Test
  public void negationOperator() {
    assertThat(NEG.operate(5)).isEqualTo(-5);
  }
}
