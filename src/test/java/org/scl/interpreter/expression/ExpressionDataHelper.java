package org.scl.interpreter.expression;

/**
 * @author Evgeny Pavlovsky
 */
public class ExpressionDataHelper {

  public static ArithmeticExpression constantArithmeticExpression(Integer value) {
    return ArithmeticExpression.builder().number(value).build();
  }

  public static ArithmeticExpression arithmeticExpression() {
    return ArithmeticExpression.builder().number(2).binaryOperator("+").number(1).build();
  }

  public static RelationalExpression relationalExpression() {
    final ArithmeticExpression n1 = ArithmeticExpression.builder().number(1).build();
    final ArithmeticExpression n2 = ArithmeticExpression.builder().number(2).build();
    return RelationalExpression.builder().leftOperand(n1).operator("<").rightOperand(n2).build();
  }
}
