package org.scl.interpreter.expression;

import org.junit.Test;
import org.scl.interpreter.ExecutionContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.scl.interpreter.expression.RelationalOperator.EQUAL;
import static org.scl.interpreter.expression.RelationalOperator.GREATER_OR_EQUAL;
import static org.scl.interpreter.expression.RelationalOperator.LESS;
import static org.scl.interpreter.expression.RelationalOperator.LESS_OR_EQUAL;
import static org.scl.interpreter.expression.RelationalOperator.NOT_EQUAL;

/**
 * @author Evgeny Pavlovsky
 */
public class RelationalExpressionTest {

  private Expression<Integer> one = ExpressionDataHelper.constantArithmeticExpression(1);
  private Expression<Integer> two = ExpressionDataHelper.constantArithmeticExpression(2);
  private Expression<Integer> three = ExpressionDataHelper.constantArithmeticExpression(3);
  private ExecutionContext context = new ExecutionContext();

  @Test
  public void greater() {
    assertThat(getResult(three, RelationalOperator.GREATER, two)).isTrue();
    assertThat(getResult(one, RelationalOperator.GREATER, two)).isFalse();
  }

  @Test
  public void less() {
    assertThat(getResult(two, LESS, three)).isTrue();
    assertThat(getResult(three, LESS, three)).isFalse();
  }

  @Test
  public void equal() {
    assertThat(getResult(one, EQUAL, one)).isTrue();
    assertThat(getResult(two, EQUAL, three)).isFalse();
  }

  @Test
  public void notEqual() {
    assertThat(getResult(one, NOT_EQUAL, two)).isTrue();
    assertThat(getResult(three, NOT_EQUAL, three)).isFalse();
  }

  @Test
  public void greaterOrEqual() {
    assertThat(getResult(one, GREATER_OR_EQUAL, one)).isTrue();
    assertThat(getResult(three, GREATER_OR_EQUAL, two)).isTrue();
    assertThat(getResult(two, GREATER_OR_EQUAL, three)).isFalse();
  }

  @Test
  public void lessOrEqual() {
    assertThat(getResult(two, LESS_OR_EQUAL, two)).isTrue();
    assertThat(getResult(one, LESS_OR_EQUAL, two)).isTrue();
    assertThat(getResult(three, LESS_OR_EQUAL, one)).isFalse();
  }

  private Boolean getResult(Expression<Integer> left, RelationalOperator operator, Expression<Integer> right) {
    return RelationalExpression.builder()
                               .leftOperand(left)
                               .rightOperand(right)
                               .operator(operator)
                               .build()
                               .execute(context);
  }
}
