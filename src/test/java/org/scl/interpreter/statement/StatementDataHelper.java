package org.scl.interpreter.statement;

/**
 * @author Evgeny Pavlovsky
 */
public class StatementDataHelper {

  public static ErrorStatement errorStatement() {
    return ErrorStatement.builder().errorMessage("Error!").build();
  }
}
