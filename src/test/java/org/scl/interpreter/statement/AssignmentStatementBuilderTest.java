package org.scl.interpreter.statement;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.scl.interpreter.error.SclTranslationException;
import org.scl.interpreter.expression.ArithmeticExpression;
import org.scl.interpreter.expression.ExpressionDataHelper;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Evgeny Pavlovsky
 */
public class AssignmentStatementBuilderTest {

  @Rule
  public ExpectedException exception = ExpectedException.none();

  private AssignmentStatementBuilder builder = new AssignmentStatementBuilder();

  @Test
  public void buildsStatement() {
    final ArithmeticExpression expression = ExpressionDataHelper.arithmeticExpression();
    final String variable = "my_var";
    AssignmentStatement statement = builder.expression(expression).variable(variable).build();
    assertThat(statement).isNotNull();
    assertThat(statement.getExpression()).isSameAs(expression);
    assertThat(statement.getVariable()).isEqualTo(variable);
  }

  @Test
  public void buildsNewInstanceEachTime() {
    final ArithmeticExpression expression = ExpressionDataHelper.arithmeticExpression();
    AssignmentStatement s1 = builder.expression(expression).variable("my_var").build();
    AssignmentStatement s2 = builder.expression(expression).variable("my_var").build();
    assertThat(s1).isNotSameAs(s2);
  }

  @Test
  public void expressionCannotBeNull() {
    exception.expect(SclTranslationException.class);
    exception.expectMessage("Missing expression.");
    builder.variable("my_var").build();
  }

  @Test
  public void variableCannotBeNull() {
    exception.expect(SclTranslationException.class);
    exception.expectMessage("Variable name cannot be null or blank.");
    ArithmeticExpression expression = ExpressionDataHelper.arithmeticExpression();
    builder.expression(expression).build();
  }

  @Test
  public void variableCannotBeBlank() {
    exception.expect(SclTranslationException.class);
    exception.expectMessage("Variable name cannot be null or blank.");
    ArithmeticExpression expression = ExpressionDataHelper.arithmeticExpression();
    builder.expression(expression).variable("   ").build();
  }
}
