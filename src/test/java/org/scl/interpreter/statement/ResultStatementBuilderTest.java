package org.scl.interpreter.statement;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.scl.interpreter.error.SclTranslationException;
import org.scl.interpreter.expression.Expression;
import org.scl.interpreter.expression.ExpressionDataHelper;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Evgeny Pavlovsky
 */
public class ResultStatementBuilderTest {
  @Rule
  public ExpectedException exception = ExpectedException.none();

  private ResultStatementBuilder builder = new ResultStatementBuilder();

  @Test
  public void buildsStatement() {
    final Expression<Integer> expression = ExpressionDataHelper.arithmeticExpression();
    ResultStatement statement = builder.expression(expression).build();
    assertThat(statement).isNotNull();
    assertThat(statement.getExpression()).isSameAs(expression);
  }

  @Test
  public void buildsNewInstanceEachTime() {
    final Expression<Integer> expression = ExpressionDataHelper.arithmeticExpression();
    ResultStatement s1 = builder.expression(expression).build();
    ResultStatement s2 = builder.expression(expression).build();
    assertThat(s1).isNotSameAs(s2);
  }

  @Test
  public void expressionCannotBeNull() {
    exception.expect(SclTranslationException.class);
    exception.expectMessage("Missing expression.");
    builder.build();
  }
}
