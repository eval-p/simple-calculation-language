package org.scl.interpreter.statement;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.scl.interpreter.error.SclTranslationException;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Evgeny Pavlovsky
 */
public class ErrorStatementBuilderTest {
  @Rule
  public ExpectedException exception = ExpectedException.none();

  private ErrorStatementBuilder builder = new ErrorStatementBuilder();

  @Test
  public void buildsStatement() {
    final String message = "This is error!";
    ErrorStatement statement = builder.errorMessage(message).build();
    assertThat(statement).isNotNull();
    assertThat(statement.getErrorMessage()).isEqualTo(message);
  }

  @Test
  public void buildsNewInstanceEachTime() {
    ErrorStatement s1 = builder.errorMessage("This is error!").build();
    ErrorStatement s2 = builder.errorMessage("This is error!").build();
    assertThat(s1).isNotSameAs(s2);
  }

  @Test
  public void errorMessageCannotBeNull() {
    exception.expect(SclTranslationException.class);
    exception.expectMessage("Error message cannot be null or blank.");

    builder.build();
  }

  @Test
  public void errorMessageCannotBeBlank() {
    exception.expect(SclTranslationException.class);
    exception.expectMessage("Error message cannot be null or blank.");

    builder.errorMessage(" ").build();
  }
}
