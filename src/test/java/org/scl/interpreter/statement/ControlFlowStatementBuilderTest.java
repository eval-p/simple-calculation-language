package org.scl.interpreter.statement;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.scl.interpreter.error.SclTranslationException;
import org.scl.interpreter.expression.ExpressionDataHelper;
import org.scl.interpreter.expression.RelationalExpression;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Evgeny Pavlovsky
 */
public class ControlFlowStatementBuilderTest {
  @Rule
  public ExpectedException exception = ExpectedException.none();

  private ControlFlowStatementBuilder builder = new ControlFlowStatementBuilder();

  @Test
  public void buildsStatement() {
    final RelationalExpression expression = ExpressionDataHelper.relationalExpression();
    final Statement innerStatement = StatementDataHelper.errorStatement();
    final ControlFlowStatement statement = builder.condition(expression).statement(innerStatement).repeatable().build();
    assertThat(statement).isNotNull();
    assertThat(statement.getCondition()).isSameAs(expression);
    assertThat(statement.isRepeatable()).isTrue();
    assertThat(statement.getStatements()).hasSize(1);
  }

  @Test
  public void statementIsNotRepeatableByDefault() {
    final RelationalExpression expression = ExpressionDataHelper.relationalExpression();
    final Statement innerStatement = StatementDataHelper.errorStatement();
    final ControlFlowStatement statement = builder.condition(expression).statement(innerStatement).build();
    assertThat(statement.isRepeatable()).isFalse();
  }

  @Test
  public void buildsNewInstanceEachTime() {
    final RelationalExpression expression = ExpressionDataHelper.relationalExpression();
    final Statement innerStatement = StatementDataHelper.errorStatement();
    final ControlFlowStatement s1 = builder.condition(expression).statement(innerStatement).build();
    final ControlFlowStatement s2 = builder.condition(expression).statement(innerStatement).build();
    assertThat(s1).isNotSameAs(s2);
  }

  @Test
  public void conditionCannotBeNull() {
    exception.expect(SclTranslationException.class);
    exception.expectMessage("Condition cannot be null.");
    final Statement innerStatement = StatementDataHelper.errorStatement();
    builder.statement(innerStatement).build();
  }

  @Test
  public void mustContainAtLeastOneStatement() {
    exception.expect(SclTranslationException.class);
    exception.expectMessage("Must contain at least one statement.");
    final RelationalExpression expression = ExpressionDataHelper.relationalExpression();
    builder.condition(expression).build();
  }
}
