package org.scl.interpreter;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.scl.interpreter.error.SclTranslationException;
import org.scl.interpreter.statement.Statement;
import org.scl.interpreter.statement.StatementDataHelper;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Evgeny Pavlovsky
 */
public class ScriptModelBuilderTest {
  @Rule
  public ExpectedException exception = ExpectedException.none();

  private ScriptModelBuilder builder = new ScriptModelBuilder();

  @Test
  public void buildsModel() {
    final String param = "my_param";
    final Statement statement = StatementDataHelper.errorStatement();
    final ScriptModel model = builder.statement(statement).parameter(param).build();
    assertThat(model).isNotNull();
    assertThat(model.getStatements()).hasSize(1).contains(statement);
    assertThat(model.getParameters()).hasSize(1).contains(param);
  }

  @Test
  public void buildsNewInstanceEachTime() {
    final Statement statement = StatementDataHelper.errorStatement();
    final ScriptModel m1 = builder.statement(statement).build();
    final ScriptModel m2 = builder.statement(statement).build();
    assertThat(m1).isNotSameAs(m2);
  }

  @Test
  public void mustContainAtLeastOneStatement() {
    exception.expect(SclTranslationException.class);
    exception.expectMessage("Model must contain at least one statement.");
    builder.build();
  }
}
