package org.scl.interpreter;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.scl.interpreter.error.SclRuntimeException;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

/**
 * @author Evgeny Pavlovsky
 */
public class ExecutionContextTest {

  private ExecutionContext context = new ExecutionContext();

  @Rule
  public ExpectedException exception = ExpectedException.none();

  @Test
  public void assignsValueToVariable() {
    final String name = "my_var";
    context.assignVariable(name, 25);
    assertThat(context.getVariableValue(name)).isEqualTo(25);
  }

  @Test
  public void allowsRepeatedAssignments() {
    final String name = "my_var";
    context.assignVariable(name, 1);
    context.assignVariable(name, 10);
    context.assignVariable(name, 7);
    assertThat(context.getVariableValue(name)).isEqualTo(7);
  }

  @Test
  public void variableValueCannotBeNull() {
    exception.expect(SclRuntimeException.class);
    exception.expectMessage("Attempt to assign empty value to variable ('var').");
    context.assignVariable("var", null);
  }

  @Test
  public void variableNameCannotBeNull() {
    exception.expect(SclRuntimeException.class);
    exception.expectMessage("Attempt to assign value (5) to unnamed variable.");
    context.assignVariable(null, 5);
  }

  @Test
  public void variableNameCannotBeEmpty() {
    exception.expect(SclRuntimeException.class);
    exception.expectMessage("Attempt to assign value (3) to unnamed variable.");
    context.assignVariable("", 3);
  }

  @Test
  public void throwsExceptionIfVariableIsNotAssigned() {
    exception.expect(SclRuntimeException.class);
    exception.expectMessage("Unknown variable 'no_such_var'.");
    context.getVariableValue("no_such_var");
  }

  @Test
  public void shouldProceedIfNothingHappened() {
    assertThat(context.shouldProceed()).isTrue();
  }

  @Test
  public void finishesExecution() {
    final int resultValue = 3;
    context.finishExecution(resultValue);
    assertThat(context.shouldProceed()).isFalse();
    assertThat(context.hasFinishedSuccessfully()).isTrue();
    assertThat(context.getResultValue()).isEqualTo(resultValue);
    assertThat(context.getErrorMessage()).isNull();
  }

  @Test
  public void abortsExecution() {
    final String errorMessage = "error";
    context.abortExecution(errorMessage);
    assertThat(context.shouldProceed()).isFalse();
    assertThat(context.hasFinishedSuccessfully()).isFalse();
    assertThat(context.getErrorMessage()).isEqualTo(errorMessage);
    assertThat(context.getResultValue()).isNull();
  }

  @Test
  public void resultValueCannotBeNull() {
    exception.expect(SclRuntimeException.class);
    exception.expectMessage("Result value cannot be null.");
    context.finishExecution(null);
  }

  @Test
  public void errorMessageCannotBeNull() {
    exception.expect(SclRuntimeException.class);
    exception.expectMessage("Error message cannot be null or blank.");
    context.abortExecution(null);
  }

  @Test
  public void errorMessageCannotBeEmpty() {
    exception.expect(SclRuntimeException.class);
    exception.expectMessage("Error message cannot be null or blank.");
    context.abortExecution("   ");
  }

}
