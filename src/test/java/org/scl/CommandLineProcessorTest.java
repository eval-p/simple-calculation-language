package org.scl;

import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.scl.TestUtil.getAbsoluteFilename;

/**
 * @author Evgeny Pavlovsky
 */
public class CommandLineProcessorTest {

  private List<String> output;

  @Test
  public void missingArguments() {
    process();
    check("Missing script name. Parameters: <script-name> [<script-arguments>].");
  }

  @Test
  public void invalidScriptName() {
    process("not_valid_path/not_valid_name.scl");
    check("Script with name 'not_valid_path/not_valid_name.scl' is not found.");
  }

  @Test
  public void invalidScriptParameters() {
    final String script = getAbsoluteFilename("/org/scl/result/fibonacci_number.scl");
    process(script, "not_valid_param");
    check("Script argument 'not_valid_param' is not valid integer.");
  }

  @Test
  public void successfulCalculation() {
    final String script = getAbsoluteFilename("/org/scl/result/fibonacci_number.scl");
    process(script, "7");
    check("Calculation successful.", "Calculation result: 13");
  }

  @Test
  public void failedCalculation() {
    final String script = getAbsoluteFilename("/org/scl/result/fibonacci_number.scl");
    process(script);
    check("Calculation failed.", "Error message: Invalid number of arguments. Expected: 1, found: 0.");
  }

  private void process(String... args) {
    CommandLineProcessor processor = new CommandLineProcessor();
    output = processor.process(args);
  }

  private void check(String... expected) {
    assertThat(output).containsExactly(expected);
  }
}
