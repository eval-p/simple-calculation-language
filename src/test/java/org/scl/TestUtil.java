package org.scl;

import java.net.URL;

/**
 * @author Evgeny Pavlovsky
 */
public class TestUtil {

  public static String getAbsoluteFilename(String relativeFilename) {
    final URL resource = TestUtil.class.getResource(relativeFilename);
    if (resource == null) {
      throw new RuntimeException("File not found: " + relativeFilename);
    }
    return resource.getFile();
  }
}
