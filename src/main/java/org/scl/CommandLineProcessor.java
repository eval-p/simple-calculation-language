package org.scl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Evgeny Pavlovsky
 */
class CommandLineProcessor {

  private List<String> output = new ArrayList<>();
  private boolean done = false;
  private String[] arguments;
  private String scriptName;
  private Integer[] scriptArguments;

  List<String> process(String[] args) {
    arguments = args;
    final List<Runnable> steps = new ArrayList<>();
    steps.add(this::checkArgumentsCount);
    steps.add(this::parseScriptName);
    steps.add(this::parseScriptArguments);
    steps.add(this::interpret);

    for (Runnable step : steps) {
      step.run();
      if (done) {
        break;
      }
    }

    return output;
  }

  private void parseScriptArguments() {
    scriptArguments = new Integer[arguments.length - 1];
    for (int i = 1; i < arguments.length; i++) {
      try {
        scriptArguments[i - 1] = Integer.valueOf(arguments[i]);
      } catch (NumberFormatException e) {
        output.add("Script argument '" + arguments[i] + "' is not valid integer.");
        done = true;
      }
    }
  }

  private void checkArgumentsCount() {
    if (arguments.length == 0) {
      output.add("Missing script name. Parameters: <script-name> [<script-arguments>].");
      done = true;
    }
  }

  private void parseScriptName() {
    scriptName = arguments[0];
    final File scriptFile = new File(scriptName);
    if (!scriptFile.exists()) {
      output.add("Script with name '" + scriptName + "' is not found.");
      done = true;
    }
  }

  private void interpret() {
    final Result result = Scl.interpretFromFile(scriptName, scriptArguments);
    if (result.isSuccessful()) {
      output.add("Calculation successful.");
      output.add("Calculation result: " + result.getValue());
    } else {
      output.add("Calculation failed.");
      output.add("Error message: " + result.getErrorMessage());
    }
  }
}
