package org.scl;

import org.antlr.v4.runtime.CharStream;
import org.scl.interpreter.Interpreter;
import org.scl.interpreter.ScriptCall;

import java.io.IOException;

import static org.antlr.v4.runtime.CharStreams.fromFileName;
import static org.antlr.v4.runtime.CharStreams.fromString;

/**
 * Utility class that contains methods for convenient SCL script interpretation.
 * Also a start point for command line interface.
 *
 * @author Evgeny Pavlovsky
 */
@SuppressWarnings("unused")
public class Scl {

  private static Interpreter interpreter = new Interpreter();

  public static void main(String[] args) {
    final CommandLineProcessor processor = new CommandLineProcessor();
    processor.process(args).forEach(System.out::println);
  }

  public static Result interpretFromFile(String filename, Integer... arguments) {
    final CharStream contentStream = getContentStreamFromFile(filename);
    return interpret(contentStream, arguments);
  }

  public static Result interpretFromString(String script, Integer... arguments) {
    final CharStream contentStream = fromString(script);
    return interpret(contentStream, arguments);
  }

  private static Result interpret(CharStream contentStream, Integer... arguments) {
    final ScriptCall call = new ScriptCall(contentStream);
    call.addArguments(arguments != null ? arguments : new Integer[0]);
    return interpreter.interpret(call);
  }

  private static CharStream getContentStreamFromFile(String filename) {
    final CharStream contentStream;
    try {
      contentStream = fromFileName(filename);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    return contentStream;
  }
}
