package org.scl.interpreter;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.scl.Result;
import org.scl.grammar.SclLexer;
import org.scl.grammar.SclParser;
import org.scl.interpreter.error.SclErrorListener;
import org.scl.interpreter.error.SclException;

import java.util.List;

/**
 * Parses and interprets SCL from {@link ScriptCall} object. Returns {@link Result}.
 *
 * @author Evgeny Pavlovsky
 */
public class Interpreter {

  public Result interpret(ScriptCall call) {
    try {
      return processScriptCall(call);
    } catch (SclException e) {
      return getErrorResult(e);
    }
  }

  private Result processScriptCall(ScriptCall call) {
    final ParseTree tree = parseScript(call.getScriptContent());
    final ScriptModel model = translateToScriptModel(tree);
    return execute(model, call.getArguments());
  }

  private ParseTree parseScript(CharStream scriptContent) {
    SclParser parser = getSclParser(scriptContent);
    return parser.sclScript();
  }

  private Result execute(ScriptModel model, List<Integer> arguments) {
    VirtualMachine vm = new VirtualMachine();
    return vm.execute(model, arguments);
  }

  private ScriptModel translateToScriptModel(ParseTree tree) {
    final ParseTreeWalker walker = new ParseTreeWalker();
    final SyntaxTreeTranslator translator = new SyntaxTreeTranslator();
    walker.walk(translator, tree);
    return translator.getModel();
  }

  private SclParser getSclParser(CharStream scriptContent) {
    final SclLexer lexer = new SclLexer(scriptContent);
    final SclParser parser = new SclParser(new CommonTokenStream(lexer));
    final SclErrorListener errorListener = new SclErrorListener();
    parser.removeErrorListeners();
    lexer.removeErrorListeners();
    parser.addErrorListener(errorListener);
    lexer.addErrorListener(errorListener);
    return parser;
  }

  private Result getErrorResult(SclException e) {
    return Result.errorResult()
                 .errorMessage(e.getMessage())
                 .get();
  }
}
