package org.scl.interpreter.error;

/**
 * @author Evgeny Pavlovsky
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class SclParsingException extends SclException {

  public SclParsingException() {
    super();
  }

  public SclParsingException(String message) {
    super(message);
  }

  public SclParsingException(String message, Throwable cause) {
    super(message, cause);
  }

  public SclParsingException(Throwable cause) {
    super(cause);
  }

  protected SclParsingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
