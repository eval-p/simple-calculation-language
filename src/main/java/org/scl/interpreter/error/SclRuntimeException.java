package org.scl.interpreter.error;

/**
 * @author Evgeny Pavlovsky
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class SclRuntimeException extends SclException {

  public SclRuntimeException() {
    super();
  }

  public SclRuntimeException(String message) {
    super(message);
  }

  public SclRuntimeException(String message, Throwable cause) {
    super(message, cause);
  }

  public SclRuntimeException(Throwable cause) {
    super(cause);
  }

  protected SclRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
