package org.scl.interpreter.error;

/**
 * @author Evgeny Pavlovsky
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class SclTranslationException extends SclException {

  public SclTranslationException() {
    super();
  }

  public SclTranslationException(String message) {
    super(message);
  }

  public SclTranslationException(String message, Throwable cause) {
    super(message, cause);
  }

  public SclTranslationException(Throwable cause) {
    super(cause);
  }

  protected SclTranslationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
