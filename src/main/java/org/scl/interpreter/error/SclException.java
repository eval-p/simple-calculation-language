package org.scl.interpreter.error;

/**
 * @author Evgeny Pavlovsky
 */
@SuppressWarnings("WeakerAccess")
public class SclException extends RuntimeException {

  public SclException() {
    super();
  }

  public SclException(String message) {
    super(message);
  }

  public SclException(String message, Throwable cause) {
    super(message, cause);
  }

  public SclException(Throwable cause) {
    super(cause);
  }

  protected SclException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
