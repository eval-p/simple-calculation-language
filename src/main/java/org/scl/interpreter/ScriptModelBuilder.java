package org.scl.interpreter;

import org.scl.interpreter.statement.Statement;
import org.scl.interpreter.validation.SclChecks;

import java.util.Collection;

/**
 * @author Evgeny Pavlovsky
 */
@SuppressWarnings("WeakerAccess")
public class ScriptModelBuilder {

  private ScriptModel model = new ScriptModel();

  public ScriptModelBuilder parameter(String parameter) {
    model.getParameters().add(parameter);
    return this;
  }

  public ScriptModelBuilder statement(Statement statement) {
    model.getStatements().add(statement);
    return this;
  }

  public ScriptModelBuilder statements(Collection<Statement> statements) {
    model.getStatements().addAll(statements);
    return this;
  }

  public ScriptModel build() {
    validate();
    final ScriptModel result = model;
    model = new ScriptModel();
    return result;
  }

  private void validate() {
    SclChecks.translation().condition(!model.getStatements().isEmpty(), "Model must contain at least one statement.");
  }
}
