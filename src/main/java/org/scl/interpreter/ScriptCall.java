package org.scl.interpreter;

import org.antlr.v4.runtime.CharStream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Context for SCL script interpretation. Contains initial data like script content and arguments.
 *
 * @author Evgeny Pavlovsky
 */
@SuppressWarnings("WeakerAccess")
public class ScriptCall {

  private List<Integer> arguments = new ArrayList<>();
  private CharStream scriptContent;

  public ScriptCall(CharStream scriptContent) {
    this.scriptContent = scriptContent;
  }

  public CharStream getScriptContent() {
    return scriptContent;
  }

  public List<Integer> getArguments() {
    return arguments;
  }

  public void addArgument(Integer param) {
    arguments.add(param);
  }

  public void addArguments(Integer... args) {
    Arrays.stream(args).forEach(arguments::add);
  }
}
