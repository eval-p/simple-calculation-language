package org.scl.interpreter;

import org.scl.Result;
import org.scl.interpreter.statement.ControlFlowStatement;
import org.scl.interpreter.statement.ExecutableStatement;
import org.scl.interpreter.statement.Statement;
import org.scl.interpreter.validation.SclChecks;

import java.util.List;
import java.util.Stack;
import java.util.function.Supplier;

/**
 * @author Evgeny Pavlovsky
 */
class VirtualMachine {

  private ExecutionContext context = new ExecutionContext();
  private Stack<StatementBlock> blocks = new Stack<>();

  Result execute(ScriptModel model, List<Integer> arguments) {
    addArgumentsToContext(model.getParameters(), arguments);
    newBlock(model.getStatements());

    Statement statement = getNextStatement();
    while (statement != null) {
      execute(statement);
      if (!context.shouldProceed()) {
        return getResult();
      }
      statement = getNextStatement();
    }

    return getErrorResult("Script execution finished, but no result was returned.");
  }

  private Statement getNextStatement() {
    while (!blocks.empty()) {
      StatementBlock block = blocks.peek();
      if (block.hasMoreStatements()) {
        return block.getNextStatement();
      } else {
        blocks.pop();
      }
    }
    return null;
  }

  private void addArgumentsToContext(List<String> parameters, List<Integer> arguments) {
    validateArgumentsCount(parameters, arguments);
    int index = 0;
    for (String param : parameters) {
      context.assignVariable(param, arguments.get(index++));
    }
  }

  private void validateArgumentsCount(List<String> parameters, List<Integer> arguments) {
    final int paramsSize = parameters.size();
    final int argsSize = arguments.size();
    boolean sizeEqual = (paramsSize == argsSize);
    String msgTemplate = "Invalid number of arguments. Expected: %s, found: %s.";
    SclChecks.runtime().condition(sizeEqual, msgTemplate, paramsSize, argsSize);
  }

  private Result getResult() {
    if (context.hasFinishedSuccessfully()) {
      return Result.successfulResult()
                   .value(context.getResultValue())
                   .get();

    } else {
      return Result.errorResult()
                   .errorMessage(context.getErrorMessage())
                   .get();
    }
  }

  private void execute(Statement statement) {
    if (statement instanceof ExecutableStatement) {
      execute((ExecutableStatement) statement);
    } else if (statement instanceof ControlFlowStatement) {
      execute((ControlFlowStatement) statement);
    }
  }

  private void execute(ExecutableStatement statement) {
    statement.execute(context);
  }

  private void execute(ControlFlowStatement statement) {
    if (!statement.executeCondition(context)) {
      return;
    }
    if (statement.isRepeatable()) {
      newRepeatableBlock(statement.getStatements(), () -> statement.executeCondition(context));
    } else {
      newBlock(statement.getStatements());
    }
  }

  private Result getErrorResult(String errorMsg) {
    return Result.errorResult().errorMessage(errorMsg).get();
  }

  private void newBlock(List<Statement> statements) {
    final StatementBlock block = new StatementBlock();
    block.statements = statements;
    blocks.push(block);
  }

  private void newRepeatableBlock(List<Statement> statements, Supplier<Boolean> condition) {
    newBlock(statements);
    blocks.peek().repeatableCondition = condition;
  }

  private static class StatementBlock {
    private List<Statement> statements;
    private int nextStatement = 0;
    private Supplier<Boolean> repeatableCondition;

    private boolean hasMoreStatements() {
      if (nextStatement < statements.size()) {
        return true;
      }
      boolean repeat = repeatableCondition != null && repeatableCondition.get();
      if (repeat) {
        nextStatement = 0;
        return true;
      }
      return false;
    }

    private Statement getNextStatement() {
      if (hasMoreStatements()) {
        return statements.get(nextStatement++);
      }
      return null;
    }
  }
}
