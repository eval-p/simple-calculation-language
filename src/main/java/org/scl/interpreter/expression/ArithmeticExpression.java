package org.scl.interpreter.expression;

import org.scl.interpreter.ExecutionContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * @author Evgeny Pavlovsky
 */
public class ArithmeticExpression implements Expression<Integer> {

  private List<Object> elements = new ArrayList<>();

  ArithmeticExpression() {
  }

  public Integer execute(ExecutionContext context) {
    final Stack<Integer> stack = new Stack<>();
    for (Object el : elements) {
      if (isNumber(el)) {
        onNumber((Integer) el, stack);
      } else if (isVariable(el)) {
        onVariable((String) el, stack, context);
      } else if (isOperator(el)) {
        onOperator((ArithmeticOperator) el, stack);
      }
    }
    return stack.pop();
  }

  private void onNumber(Integer value, Stack<Integer> stack) {
    stack.push(value);
  }

  private void onVariable(String variable, Stack<Integer> stack, ExecutionContext context) {
    Integer value = context.getVariableValue(variable);
    stack.push(value);
  }

  private void onOperator(ArithmeticOperator operator, Stack<Integer> stack) {
    final int numberOfOperands = operator.getNumberOfOperands();
    final Integer args[] = new Integer[numberOfOperands];
    for (int i = numberOfOperands - 1; i >= 0; i--) {
      args[i] = stack.pop();
    }
    Integer opResult = operator.operate(args);
    stack.push(opResult);
  }

  List<Object> getElements() {
    return elements;
  }

  private boolean isVariable(Object el) {
    return el instanceof String;
  }

  private boolean isOperator(Object el) {
    return el instanceof ArithmeticOperator;
  }

  private boolean isNumber(Object el) {
    return el instanceof Integer;
  }

  public static ArithmeticExpressionBuilder builder() {
    return new ArithmeticExpressionBuilder();
  }
}
