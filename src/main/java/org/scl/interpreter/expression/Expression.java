package org.scl.interpreter.expression;

import org.scl.interpreter.ExecutionContext;

/**
 * @author Evgeny Pavlovsky
 */
public interface Expression<T> {

  T execute(ExecutionContext context);
}
