package org.scl.interpreter.expression;

import org.scl.interpreter.validation.SclChecks;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * @author Evgeny Pavlovsky
 */
public enum ArithmeticOperator {

  NEG("-", 1, 1, args -> -args[0]),
  ADD("+", 1, args -> args[0] + args[1]),
  SUB("-", 1, args -> args[0] - args[1]),
  MUL("*", 2, args -> args[0] * args[1]),
  DIV("/", 2, args -> args[0] / args[1]);

  private int priority;
  private int numberOfOperands;
  private String symbol;
  private Function<Integer[], Integer> function;

  private static Map<String, ArithmeticOperator> binaryOperatorMap = new HashMap<>();
  private static Map<String, ArithmeticOperator> unaryOperatorMap = new HashMap<>();

  static {
    for (ArithmeticOperator o : values()) {
      Map<String, ArithmeticOperator> map = o.numberOfOperands == 1 ? unaryOperatorMap : binaryOperatorMap;
      map.put(o.symbol, o);
    }
  }

  ArithmeticOperator(String symbol, int priority, Function<Integer[], Integer> function) {
    //most of operators have 2 operands
    this(symbol, priority, 2, function);
  }

  ArithmeticOperator(String symbol, int priority, int numberOfOperands, Function<Integer[], Integer> function) {
    this.numberOfOperands = numberOfOperands;
    this.function = function;
    this.priority = priority;
    this.symbol = symbol;
  }

  public static ArithmeticOperator getBinaryOperator(String symbol) {
    return binaryOperatorMap.get(symbol);
  }

  public static ArithmeticOperator getUnaryOperator(String symbol) {
    return unaryOperatorMap.get(symbol);
  }

  public String getSymbol() {
    return symbol;
  }

  public Integer operate(Integer... args) {
    checkArgumentsCount(args);
    return function.apply(args);
  }

  public int getPriority() {
    return priority;
  }

  public int getNumberOfOperands() {
    return numberOfOperands;
  }

  private void checkArgumentsCount(Integer... args) {
    SclChecks.runtime().condition(args.length == numberOfOperands,
      "Incorrect arguments count. Actual: %s, expected: %s.", args.length, numberOfOperands);
  }
}
