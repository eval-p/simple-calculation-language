package org.scl.interpreter.expression;

import org.scl.interpreter.error.SclTranslationException;
import org.scl.interpreter.validation.SclChecks;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import static java.lang.Integer.valueOf;
import static java.lang.String.format;

/**
 * @author Evgeny Pavlovsky
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class ArithmeticExpressionBuilder {
  private static final String OPENING_BRACKET = "(";
  private static final String CLOSING_BRACKET = ")";

  private List<Object> elements = new ArrayList<>();
  private Stack<Object> stack = new Stack<>();
  private StringBuilder originalExpression = new StringBuilder();

  public ArithmeticExpressionBuilder number(Integer number) {
    originalExpression.append(number);
    elements.add(number);
    return this;
  }

  public ArithmeticExpressionBuilder number(String number) {
    Integer convertedNumber = valueOf(number);
    return number(convertedNumber);
  }

  public ArithmeticExpressionBuilder operator(ArithmeticOperator operator) {
    originalExpression.append(operator.getSymbol());
    while (stackTopHashHigherPriority(operator)) {
      elements.add(stack.pop());
    }
    stack.push(operator);
    return this;
  }

  public ArithmeticExpressionBuilder binaryOperator(String operator) {
    ArithmeticOperator convertedOperator = ArithmeticOperator.getBinaryOperator(operator);
    return operator(convertedOperator);
  }

  public ArithmeticExpressionBuilder unaryOperator(String operator) {
    ArithmeticOperator convertedOperator = ArithmeticOperator.getUnaryOperator(operator);
    return operator(convertedOperator);
  }

  public ArithmeticExpressionBuilder add() {
    return operator(ArithmeticOperator.ADD);
  }

  public ArithmeticExpressionBuilder sub() {
    return operator(ArithmeticOperator.SUB);
  }

  public ArithmeticExpressionBuilder mul() {
    return operator(ArithmeticOperator.MUL);
  }

  public ArithmeticExpressionBuilder div() {
    return operator(ArithmeticOperator.DIV);
  }

  public ArithmeticExpressionBuilder neg() {
    return operator(ArithmeticOperator.NEG);
  }

  public ArithmeticExpressionBuilder variable(String variable) {
    SclChecks.translation().variableName(variable);
    originalExpression.append(variable);
    elements.add(variable);
    return this;
  }

  public ArithmeticExpressionBuilder openingBracket() {
    originalExpression.append(OPENING_BRACKET);
    stack.push(OPENING_BRACKET);
    return this;
  }

  public ArithmeticExpressionBuilder closingBracket() {
    originalExpression.append(CLOSING_BRACKET);
    popAllOperatorsBeforeOpeningBracket();
    return this;
  }

  public ArithmeticExpression build() {
    moveAllOperatorsFromStack();
    final ArithmeticExpression expression = createExpression();
    validate(expression);
    reset();
    return expression;
  }

  private ArithmeticExpression createExpression() {
    final ArithmeticExpression expression = new ArithmeticExpression();
    expression.getElements().addAll(elements);
    return expression;
  }

  private void reset() {
    elements.clear();
    originalExpression = new StringBuilder();
  }

  private void popAllOperatorsBeforeOpeningBracket() {
    while (!stack.isEmpty()) {
      Object operator = stack.pop();
      if (operator == OPENING_BRACKET) {
        return;
      }
      elements.add(operator);
    }
    errorOnMissingBracket("opening");
  }

  private void moveAllOperatorsFromStack() {
    while (!stack.isEmpty()) {
      Object operator = stack.pop();
      checkIsNotOpeningBracket(operator);
      elements.add(operator);
    }
  }

  private boolean stackTopHashHigherPriority(ArithmeticOperator operator) {
    if (stack.isEmpty()) {
      return false;
    }
    final Object topObject = stack.peek();
    if (!(topObject instanceof ArithmeticOperator)) {
      return false;
    }
    ArithmeticOperator top = (ArithmeticOperator) topObject;
    return top.getPriority() >= operator.getPriority();
  }

  private void checkIsNotOpeningBracket(Object operator) {
    if (operator == OPENING_BRACKET) {
      errorOnMissingBracket("closing");
    }
  }

  private void errorOnMissingBracket(String bracketType) {
    String template = "Missing %s bracket in arithmetic expression '%s'.";
    throw new SclTranslationException(format(template, bracketType, originalExpression));
  }

  private void validate(ArithmeticExpression expression) {
    checkNotEmpty(expression);
    checkHasEnoughOperands(expression);
  }

  private void checkHasEnoughOperands(ArithmeticExpression expression) {
    final Integer requiredOperandsCount = expression.getElements().stream().filter(this::isOperator)
                                                    .map(e -> ((ArithmeticOperator) e).getNumberOfOperands())
                                                    .reduce(1, (i, o) -> i + o - 1);

    final long operandsCount = expression.getElements().stream().filter(this::isOperand).count();
    SclChecks.translation().condition(operandsCount == requiredOperandsCount,
      "Incorrect expression: '%s'.", originalExpression);
  }

  private void checkNotEmpty(ArithmeticExpression expression) {
    SclChecks.translation().condition(!expression.getElements().isEmpty(), "Expression cannot be empty.");
  }

  private boolean isOperand(Object o) {
    return o instanceof Number || o instanceof String;
  }

  private boolean isOperator(Object o) {
    return o instanceof ArithmeticOperator;
  }
}
