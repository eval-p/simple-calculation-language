package org.scl.interpreter.expression;

import org.scl.interpreter.ExecutionContext;

/**
 * @author Evgeny Pavlovsky
 */
public class RelationalExpression implements Expression<Boolean> {

  private RelationalOperator operator;
  private Expression<Integer> leftOperand;
  private Expression<Integer> rightOperand;

  @Override
  public Boolean execute(ExecutionContext context) {
    final Integer leftArg = leftOperand.execute(context);
    final Integer rightArg = rightOperand.execute(context);
    return operator.operate(leftArg, rightArg);
  }

  public static RelationalExpressionBuilder builder() {
    return new RelationalExpressionBuilder();
  }

  RelationalOperator getOperator() {
    return operator;
  }

  void setOperator(RelationalOperator operator) {
    this.operator = operator;
  }

  Expression<Integer> getLeftOperand() {
    return leftOperand;
  }

  void setLeftOperand(Expression<Integer> leftOperand) {
    this.leftOperand = leftOperand;
  }

  Expression<Integer> getRightOperand() {
    return rightOperand;
  }

  void setRightOperand(Expression<Integer> rightOperand) {
    this.rightOperand = rightOperand;
  }
}
