package org.scl.interpreter.expression;

import org.scl.interpreter.validation.SclChecks;

/**
 * @author Evgeny Pavlovsky
 */
public class RelationalExpressionBuilder {

  private RelationalExpression expression = new RelationalExpression();

  public RelationalExpressionBuilder operator(String operator) {
    return operator(RelationalOperator.fromString(operator));
  }

  public RelationalExpressionBuilder operator(RelationalOperator operator) {
    expression.setOperator(operator);
    return this;
  }

  public RelationalExpressionBuilder leftOperand(Expression<Integer> expression) {
    this.expression.setLeftOperand(expression);
    return this;
  }

  public RelationalExpressionBuilder rightOperand(Expression<Integer> expression) {
    this.expression.setRightOperand(expression);
    return this;
  }

  public RelationalExpression build() {
    validate();
    final RelationalExpression result = expression;
    expression = new RelationalExpression();
    return result;
  }

  private void validate() {
    SclChecks.translation().notNull(expression.getOperator(), "Operator cannot be null.");
    SclChecks.translation().notNull(expression.getLeftOperand(), "Left operand cannot be null.");
    SclChecks.translation().notNull(expression.getRightOperand(), "Right operand cannot be null.");
  }
}
