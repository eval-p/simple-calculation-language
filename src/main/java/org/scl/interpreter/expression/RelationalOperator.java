package org.scl.interpreter.expression;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

/**
 * @author Evgeny Pavlovsky
 */
public enum RelationalOperator {

  EQUAL("==", (a, b) -> a.compareTo(b) == 0),
  NOT_EQUAL("!=", (a, b) -> a.compareTo(b) != 0),
  GREATER(">", (a, b) -> a.compareTo(b) > 0),
  LESS("<", (a, b) -> a.compareTo(b) < 0),
  GREATER_OR_EQUAL(">=", (a, b) -> a.compareTo(b) >= 0),
  LESS_OR_EQUAL("<=", (a, b) -> a.compareTo(b) <= 0);

  private String symbol;
  private BiFunction<Integer, Integer, Boolean> function;

  private static Map<String, RelationalOperator> operatorMap = new HashMap<>();

  static {
    Arrays.stream(values()).forEach(o -> operatorMap.put(o.symbol, o));
  }

  RelationalOperator(String symbol, BiFunction<Integer, Integer, Boolean> function) {
    this.symbol = symbol;
    this.function = function;
  }

  public static RelationalOperator fromString(String operator) {
    return operatorMap.get(operator);
  }

  public boolean operate(Integer a, Integer b) {
    return function.apply(a, b);
  }

}
