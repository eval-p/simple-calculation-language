package org.scl.interpreter.statement;

import org.scl.interpreter.ExecutionContext;

/**
 * @author Evgeny Pavlovsky
 */
public class ErrorStatement implements ExecutableStatement {

  private String errorMessage;

  ErrorStatement() {
  }

  @Override
  public void execute(ExecutionContext context) {
    context.abortExecution(errorMessage);
  }

  public static ErrorStatementBuilder builder() {
    return new ErrorStatementBuilder();
  }

  void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  String getErrorMessage() {
    return errorMessage;
  }
}
