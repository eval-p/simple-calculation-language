package org.scl.interpreter.statement;

import org.scl.interpreter.ExecutionContext;
import org.scl.interpreter.expression.Expression;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Evgeny Pavlovsky
 */
public class ControlFlowStatement implements Statement {

  private Expression<Boolean> condition;
  private List<Statement> statements = new ArrayList<>();
  private boolean repeatable;

  ControlFlowStatement() {
  }

  public boolean executeCondition(ExecutionContext context) {
    return condition.execute(context);
  }

  public List<Statement> getStatements() {
    return statements;
  }

  public boolean isRepeatable() {
    return repeatable;
  }

  void setRepeatable(boolean repeatable) {
    this.repeatable = repeatable;
  }

  Expression<Boolean> getCondition() {
    return condition;
  }

  void setCondition(Expression<Boolean> condition) {
    this.condition = condition;
  }

  public static ControlFlowStatementBuilder builder() {
    return new ControlFlowStatementBuilder();
  }
}
