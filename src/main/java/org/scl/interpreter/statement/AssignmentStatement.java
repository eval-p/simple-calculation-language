package org.scl.interpreter.statement;

import org.scl.interpreter.ExecutionContext;
import org.scl.interpreter.expression.Expression;

/**
 * @author Evgeny Pavlovsky
 */
public class AssignmentStatement implements ExecutableStatement {

  private String variable;
  private Expression<Integer> expression;

  AssignmentStatement() {
  }

  @Override
  public void execute(ExecutionContext context) {
    final Integer integer = expression.execute(context);
    context.assignVariable(variable, integer);
  }

  void setVariable(String variable) {
    this.variable = variable;
  }

  void setExpression(Expression<Integer> expression) {
    this.expression = expression;
  }

  String getVariable() {
    return variable;
  }

  Expression<Integer> getExpression() {
    return expression;
  }

  public static AssignmentStatementBuilder builder() {
    return new AssignmentStatementBuilder();
  }
}
