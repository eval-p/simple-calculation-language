package org.scl.interpreter.statement;

import org.scl.interpreter.expression.Expression;
import org.scl.interpreter.validation.SclChecks;

/**
 * @author Evgeny Pavlovsky
 */
public class AssignmentStatementBuilder {

  private AssignmentStatement statement = new AssignmentStatement();

  public AssignmentStatementBuilder variable(String variable) {
    statement.setVariable(variable);
    return this;
  }

  public AssignmentStatementBuilder expression(Expression<Integer> expression) {
    statement.setExpression(expression);
    return this;
  }

  public AssignmentStatement build() {
    validate();
    AssignmentStatement result = statement;
    statement = new AssignmentStatement();
    return result;
  }

  private void validate() {
    SclChecks.translation().notNull(statement.getExpression(), "Missing expression.");
    SclChecks.translation().variableName(statement.getVariable());
  }
}
