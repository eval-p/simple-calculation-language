package org.scl.interpreter.statement;

import org.scl.interpreter.expression.Expression;
import org.scl.interpreter.validation.SclChecks;

import java.util.Collection;

/**
 * @author Evgeny Pavlovsky
 */
public class ControlFlowStatementBuilder {

  private ControlFlowStatement statement = new ControlFlowStatement();

  public ControlFlowStatementBuilder condition(Expression<Boolean> condition) {
    statement.setCondition(condition);
    return this;
  }

  public ControlFlowStatementBuilder statement(Statement statement) {
    this.statement.getStatements().add(statement);
    return this;
  }

  public ControlFlowStatementBuilder statements(Collection<Statement> statements) {
    statement.getStatements().addAll(statements);
    return this;
  }

  public ControlFlowStatementBuilder repeatable() {
    statement.setRepeatable(true);
    return this;
  }

  public ControlFlowStatement build() {
    validate();
    final ControlFlowStatement result = statement;
    statement = new ControlFlowStatement();
    return result;
  }

  private void validate() {
    SclChecks.translation().notNull(statement.getCondition(), "Condition cannot be null.");
    SclChecks.translation().condition(!statement.getStatements().isEmpty(), "Must contain at least one statement.");
  }
}
