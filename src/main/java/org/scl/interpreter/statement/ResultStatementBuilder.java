package org.scl.interpreter.statement;

import org.scl.interpreter.expression.Expression;
import org.scl.interpreter.validation.SclChecks;

/**
 * @author Evgeny Pavlovsky
 */
public class ResultStatementBuilder {

  private ResultStatement statement = new ResultStatement();

  public ResultStatementBuilder expression(Expression<Integer> expression) {
    statement.setExpression(expression);
    return this;
  }

  public ResultStatement build() {
    validate();
    ResultStatement result = statement;
    statement = new ResultStatement();
    return result;
  }

  private void validate() {
    SclChecks.translation().notNull(statement.getExpression(), "Missing expression.");
  }
}
