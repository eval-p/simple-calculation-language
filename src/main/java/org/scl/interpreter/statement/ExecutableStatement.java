package org.scl.interpreter.statement;

import org.scl.interpreter.ExecutionContext;

/**
 * @author Evgeny Pavlovsky
 */
public interface ExecutableStatement extends Statement {

  void execute(ExecutionContext context);
}
