package org.scl.interpreter.statement;

import org.scl.interpreter.validation.SclChecks;

/**
 * @author Evgeny Pavlovsky
 */
public class ErrorStatementBuilder {

  private ErrorStatement statement = new ErrorStatement();

  public ErrorStatementBuilder errorMessage(String errorMessage) {
    statement.setErrorMessage(errorMessage);
    return this;
  }

  public ErrorStatement build() {
    validate();
    ErrorStatement result = statement;
    statement = new ErrorStatement();
    return result;
  }

  private void validate() {
    SclChecks.translation().notNullOrBlank(statement.getErrorMessage(), "Error message cannot be null or blank.");
  }
}
