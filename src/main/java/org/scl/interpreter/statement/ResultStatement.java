package org.scl.interpreter.statement;

import org.scl.interpreter.ExecutionContext;
import org.scl.interpreter.expression.Expression;

/**
 * @author Evgeny Pavlovsky
 */
public class ResultStatement implements ExecutableStatement {

  private Expression<Integer> expression;

  ResultStatement() {
  }

  @Override
  public void execute(ExecutionContext context) {
    final Integer resultValue = expression.execute(context);
    context.finishExecution(resultValue);
  }

  void setExpression(Expression<Integer> expression) {
    this.expression = expression;
  }

  Expression<Integer> getExpression() {
    return expression;
  }

  public static ResultStatementBuilder builder() {
    return new ResultStatementBuilder();
  }
}
