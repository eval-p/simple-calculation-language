package org.scl.interpreter;

import org.scl.interpreter.statement.Statement;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Evgeny Pavlovsky
 */
@SuppressWarnings("WeakerAccess")
public class ScriptModel {

  private List<String> parameters = new ArrayList<>();
  private List<Statement> statements = new ArrayList<>();

  List<String> getParameters() {
    return parameters;
  }

  List<Statement> getStatements() {
    return statements;
  }
}
