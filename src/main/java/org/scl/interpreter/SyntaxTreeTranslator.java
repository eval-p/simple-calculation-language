package org.scl.interpreter;

import org.antlr.v4.runtime.tree.TerminalNode;
import org.scl.grammar.SclBaseListener;
import org.scl.grammar.SclParser;
import org.scl.grammar.SclParser.ArithmeticOperandContext;
import org.scl.grammar.SclParser.ParameterDeclarationContext;
import org.scl.grammar.SclParser.VariableAssignmentContext;
import org.scl.interpreter.expression.ArithmeticExpression;
import org.scl.interpreter.expression.ArithmeticExpressionBuilder;
import org.scl.interpreter.expression.Expression;
import org.scl.interpreter.expression.RelationalExpression;
import org.scl.interpreter.statement.AssignmentStatement;
import org.scl.interpreter.statement.ControlFlowStatement;
import org.scl.interpreter.statement.ErrorStatement;
import org.scl.interpreter.statement.ResultStatement;
import org.scl.interpreter.statement.Statement;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * @author Evgeny Pavlovsky
 */
class SyntaxTreeTranslator extends SclBaseListener {

  private ScriptModelBuilder modelBuilder = new ScriptModelBuilder();
  private ArithmeticExpressionBuilder arithmeticExprBuilder = new ArithmeticExpressionBuilder();

  private Stack<Expression<?>> expressionsStack = new Stack<>();
  private Stack<List<Statement>> statementBlocks = new Stack<>();

  @Override
  public void enterCalculationBody(SclParser.CalculationBodyContext ctx) {
    newBlock(); //root statements
  }

  @Override
  public void exitCalculationBody(SclParser.CalculationBodyContext ctx) {
    List<Statement> rootStatements = statementBlocks.get(0);
    modelBuilder.statements(rootStatements);
  }

  @Override
  public void exitParameterDeclaration(ParameterDeclarationContext ctx) {
    TerminalNode identifier = ctx.variable().Identifier();
    modelBuilder.parameter(identifier.getText());
  }

  @Override
  public void exitVariableAssignment(VariableAssignmentContext ctx) {
    final ArithmeticExpression expression = (ArithmeticExpression) expressionsStack.pop();
    final String variable = ctx.variable().getText();
    final AssignmentStatement statement = AssignmentStatement.builder()
                                                             .variable(variable)
                                                             .expression(expression)
                                                             .build();
    addStatement(statement);
  }

  @Override
  public void enterIfStatement(SclParser.IfStatementContext ctx) {
    newBlock();
  }

  @Override
  public void exitIfStatement(SclParser.IfStatementContext ctx) {
    final RelationalExpression expression = (RelationalExpression) expressionsStack.pop();
    final List<Statement> statements = statementBlocks.pop();
    final ControlFlowStatement statement = ControlFlowStatement.builder()
                                                               .condition(expression)
                                                               .statements(statements)
                                                               .build();
    addStatement(statement);
  }

  @Override
  public void enterWhileStatement(SclParser.WhileStatementContext ctx) {
    newBlock();
  }

  @Override
  public void exitWhileStatement(SclParser.WhileStatementContext ctx) {
    final RelationalExpression expression = (RelationalExpression) expressionsStack.pop();
    final List<Statement> statements = statementBlocks.pop();
    final ControlFlowStatement statement = ControlFlowStatement.builder()
                                                               .condition(expression)
                                                               .statements(statements)
                                                               .repeatable()
                                                               .build();
    addStatement(statement);
  }

  @Override
  public void exitRelationalExpression(SclParser.RelationalExpressionContext ctx) {
    final String operator = ctx.relationalOperator().getText();
    final Expression<Integer> rightOperand = (ArithmeticExpression) expressionsStack.pop();
    final Expression<Integer> leftOperand = (ArithmeticExpression) expressionsStack.pop();
    final RelationalExpression expression = RelationalExpression.builder()
                                                                .operator(operator)
                                                                .leftOperand(leftOperand)
                                                                .rightOperand(rightOperand)
                                                                .build();
    expressionsStack.push(expression);
  }

  @Override
  public void exitArithmeticExpression(SclParser.ArithmeticExpressionContext ctx) {
    ArithmeticExpression expression = arithmeticExprBuilder.build();
    expressionsStack.push(expression);
  }

  @Override
  public void exitResultStatement(SclParser.ResultStatementContext ctx) {
    final ArithmeticExpression expression = (ArithmeticExpression) expressionsStack.pop();
    final ResultStatement statement = ResultStatement.builder()
                                                     .expression(expression)
                                                     .build();
    addStatement(statement);
  }

  @Override
  public void exitErrorStatement(SclParser.ErrorStatementContext ctx) {
    final String text = ctx.errorMessage().StringLiteral().getText();
    final String errorMsg = text.substring(1, text.length() - 1); //remove quotes
    final ErrorStatement statement = ErrorStatement.builder()
                                                   .errorMessage(errorMsg)
                                                   .build();
    addStatement(statement);
  }

  @Override
  public void enterArithmeticOperand(ArithmeticOperandContext ctx) {
    final String text = ctx.getText();
    if (isVariable(ctx)) {
      arithmeticExprBuilder.variable(text);
    } else if (isNumber(ctx)) {
      arithmeticExprBuilder.number(text);
    }
  }

  @Override
  public void enterOpeningBracket(SclParser.OpeningBracketContext ctx) {
    arithmeticExprBuilder.openingBracket();
  }

  @Override
  public void enterClosingBracket(SclParser.ClosingBracketContext ctx) {
    arithmeticExprBuilder.closingBracket();
  }

  @Override
  public void enterBinaryArithmeticOperator(SclParser.BinaryArithmeticOperatorContext ctx) {
    arithmeticExprBuilder.binaryOperator(ctx.getText());
  }

  @Override
  public void enterUnaryArithmeticOperator(SclParser.UnaryArithmeticOperatorContext ctx) {
    arithmeticExprBuilder.unaryOperator(ctx.getText());
  }

  ScriptModel getModel() {
    return modelBuilder.build();
  }

  private boolean isVariable(ArithmeticOperandContext ctx) {
    return ctx.variable() != null;
  }

  private boolean isNumber(ArithmeticOperandContext ctx) {
    return ctx.Number() != null;
  }

  private void addStatement(Statement statement) {
    statementBlocks.peek().add(statement);
  }

  private void newBlock() {
    statementBlocks.push(new ArrayList<>());
  }
}
