package org.scl.interpreter.validation;

import org.scl.interpreter.error.SclException;
import org.scl.interpreter.error.SclParsingException;
import org.scl.interpreter.error.SclRuntimeException;
import org.scl.interpreter.error.SclTranslationException;

/**
 * @author Evgeny Pavlovsky
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public final class SclChecks {

  private static final ParsingChecks PARSING_CHECKS = new ParsingChecks();
  private static final TranslationChecks TRANSLATION_CHECKS = new TranslationChecks();
  private static final RuntimeChecks RUNTIME_CHECKS = new RuntimeChecks();

  private SclChecks() {
  }

  public static ParsingChecks parsing() {
    return PARSING_CHECKS;
  }

  public static TranslationChecks translation() {
    return TRANSLATION_CHECKS;
  }

  public static RuntimeChecks runtime() {
    return RUNTIME_CHECKS;
  }

  abstract static class Checks {

    public void notNull(Object value, String errorMsgTemplate, Object... errorMsgArgs) {
      condition(value != null, errorMsgTemplate, errorMsgArgs);
    }

    public void notNullOrBlank(String value, String errorMsgTemplate, Object... errorMsgArgs) {
      condition(value != null && !value.trim().isEmpty(), errorMsgTemplate, errorMsgArgs);
    }

    /**
     * Throws exception if {@param condition} is false.
     */
    public void condition(boolean condition, String messageTemplate, Object... errorMsgArgs) {
      if (!condition) {
        error(messageTemplate, errorMsgArgs);
      }
    }

    public void variableName(String variable) {
      notNullOrBlank(variable, "Variable name cannot be null or blank.");
    }

    private void error(String msgTemplate, Object... args) {
      String message = String.format(msgTemplate, args);
      throw instantiateException(message);
    }

    abstract SclException instantiateException(String message);

  }

  public static class ParsingChecks extends Checks {
    @Override
    SclException instantiateException(String message) {
      return new SclParsingException(message);
    }
  }

  public static class TranslationChecks extends Checks {
    @Override
    SclException instantiateException(String message) {
      return new SclTranslationException(message);
    }
  }

  public static class RuntimeChecks extends Checks {
    @Override
    SclException instantiateException(String message) {
      return new SclRuntimeException(message);
    }
  }
}
