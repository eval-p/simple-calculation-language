package org.scl.interpreter;

import org.scl.interpreter.validation.SclChecks;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Evgeny Pavlovsky
 */
public class ExecutionContext {

  private Map<String, Integer> variablesMap = new HashMap<>();
  private boolean proceed = true;
  private Integer resultValue;
  private String errorMessage;

  public void assignVariable(String name, Integer value) {
    SclChecks.runtime().notNullOrBlank(name, "Attempt to assign value (%s) to unnamed variable.", value);
    SclChecks.runtime().notNull(value, "Attempt to assign empty value to variable ('%s').", name);
    variablesMap.put(name, value);
  }

  public Integer getVariableValue(String name) {
    final Integer value = variablesMap.get(name);
    SclChecks.runtime().notNull(value, "Unknown variable '%s'.", name);
    return value;
  }

  public void finishExecution(Integer resultValue) {
    SclChecks.runtime().notNull(resultValue, "Result value cannot be null.");
    proceed = false;
    this.resultValue = resultValue;
  }

  public void abortExecution(String errorMessage) {
    SclChecks.runtime().notNullOrBlank(errorMessage, "Error message cannot be null or blank.");
    proceed = false;
    this.errorMessage = errorMessage;
  }

  boolean shouldProceed() {
    return proceed;
  }

  boolean hasFinishedSuccessfully() {
    return errorMessage == null;
  }

  Integer getResultValue() {
    return resultValue;
  }

  String getErrorMessage() {
    return errorMessage;
  }
}
