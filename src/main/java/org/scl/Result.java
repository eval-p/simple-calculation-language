package org.scl;

/**
 * Result of SCL script interpretation. Contains either result value or error message.
 *
 * @author Evgeny Pavlovsky
 */
@SuppressWarnings("WeakerAccess")
public class Result {

  private boolean successful;
  private Integer value;
  private String errorMessage;

  Result(boolean successful) {
    this.successful = successful;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public boolean isSuccessful() {
    return successful;
  }

  public Integer getValue() {
    return value;
  }

  public static ErrorResultBuilder errorResult() {
    return new ErrorResultBuilder();
  }

  public static SuccessfulResultBuilder successfulResult() {
    return new SuccessfulResultBuilder();
  }

  public static class ErrorResultBuilder {

    private Result result = new Result(false);

    public ErrorResultBuilder errorMessage(String errorMessage) {
      result.errorMessage = errorMessage;
      return this;
    }

    public Result get() {
      return result;
    }
  }

  public static class SuccessfulResultBuilder {

    private Result result = new Result(true);

    public SuccessfulResultBuilder value(Integer value) {
      result.value = value;
      return this;
    }

    public Result get() {
      return result;
    }
  }
}
