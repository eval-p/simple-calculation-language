grammar Scl;

sclScript
    : parameterDeclaration*
      calculationBody
      EOF
    ;

parameterDeclaration
    : Parameter variable Semicolon
    ;

calculationBody
    : statement+
    ;

statement
    : variableAssignment
    | resultStatement
    | errorStatement
    | ifStatement
    | whileStatement
    ;

variableAssignment
    : variable Assign arithmeticExpression Semicolon
    ;

resultStatement
    : Result arithmeticExpression Semicolon
    ;

errorStatement
    : Error errorMessage Semicolon
    ;

ifStatement
    : If LeftParenthesis condition RightParenthesis LeftBrace statement+ RightBrace
    ;

whileStatement
    : While LeftParenthesis condition RightParenthesis LeftBrace statement+ RightBrace
    ;

condition
    : relationalExpression
    ;

errorMessage
    :  StringLiteral
    ;

relationalExpression
    : arithmeticExpression relationalOperator arithmeticExpression
    ;

arithmeticExpression
    : arithmeticSubExpression
    ;

arithmeticSubExpression
    : arithmeticSubExpression binaryArithmeticOperator arithmeticSubExpression
    | unaryArithmeticOperator arithmeticSubExpression
    | openingBracket arithmeticSubExpression closingBracket
    | arithmeticOperand
    ;

openingBracket
    : LeftParenthesis
    ;

closingBracket
    : RightParenthesis
    ;

variable
    : Identifier
    ;

arithmeticOperand
    : variable
    | Number
    ;

unaryArithmeticOperator
    : Minus
    ;

binaryArithmeticOperator
    : Plus
    | Minus
    | Mul
    | Div
    ;

relationalOperator
    : Equal
    | NotEqual
    | Greater
    | Less
    | GreaterOrEqual
    | LessOrEqual
    ;


// Keywords

Parameter     : 'parameter';
Result        : 'result';
Error         : 'error';
If            : 'if';
While         : 'while';


// Separators

LeftParenthesis           : '(';
RightParenthesis          : ')';
LeftBrace                 : '{';
RightBrace                : '}';
Semicolon                 : ';';


// Operators

Assign          : ':=';
Equal           : '==';
NotEqual        : '!=';
Greater         : '>';
Less            : '<';
GreaterOrEqual  : '>=';
LessOrEqual     : '<=';
Plus            : '+';
Minus           : '-';
Mul             : '*';
Div             : '/';


Identifier
    : Letter (Letter | Underscore | Digit)*
    ;

Number
    : NonZeroDigit Digit*
    | Zero
    ;

StringLiteral
    : Quotation .*? Quotation
    ;

Quotation         : '\'';


// Whitespace and comments

WS  :  [ \t\r\n\u000C]+ -> skip
    ;

COMMENT
    :   '/*' .*? '*/' -> channel(HIDDEN)
    ;

LINE_COMMENT
    :   '//' ~[\r\n]* -> channel(HIDDEN)
    ;



fragment Letter
    : [a-z]
    | [A-Z]
    ;

fragment Digit
    : Zero
    | NonZeroDigit
    ;

fragment NonZeroDigit
    : [1-9]
    ;

fragment Zero
    : '0'
    ;

fragment Underscore
    : '_'
    ;
