# Disclaimer #

The idea of this project is to get some experience with [_ANTLR_](http://www.antlr.org/). 

***SCL*** (_Simple Calculation Language_) is just an example to showcase what ANTLR could be used for. 
The language itself is pretty useless and is created _just for fun_. 

No claims to a good performance, original idea and support of all features that are necessary 
for a decent programming language. 
I bet there are some bugs there as well.
Nobody should ever write anything serious on SCL.

----

# Language documentation #

SCL is a ***script language*** that is ***interpreted*** on the fly, no compilation is required. 
SCL is running inside a Java Virtual Machine. 
Language syntax is pretty easy to understand, it is mostly a C-like syntax.

## SCL script ##

SCL script consist of statements that are executed sequentially one by one. 
Script can receive arguments and operate with it. 
As a result of execution, script returns result value or error message.
SCL script files usually have _.scl_ file extension.

## Comments ##

SCL supports both single-line and multiline comments.

***Example***
```javascript
//single line comment

/*multi
  line
  comment*/
```

## Data types ##

Currently SCL supports only one data type - _integer_.


## Keywords ##
| Keyword         | Description                          |
|:---------------:|:-------------------------------------|
| ***parameter*** | Used for parameter declaration       |
| ***result***    | Used for returning result            |
| ***error***     | Used for raising an error            |
| ***if***        | Used in if statement                 |
| ***while***     | Used in while statement              |


## Operators ##

### Arithmetic ###
| Operator| Description                          |
|:-------:|:-------------------------------------|
| `+`     | **Addition**, binary                 |
| `-`     | **Subtraction**, binary              |
| `*`     | **Multiplication**, binary           |
| `/`     | **Division**, binary                 |
| `-`     | **Negation**, unary                  |
      
### Relational ###
| Operator| Description                          |
|:-------:|:-------------------------------------|
| `>`     | **Greater than**, binary             |
| `<`     | **Less than**, binary                |
| `>=`    | **Greater than or equal to**, binary |
| `<=`    | **Less than or equal to**, binary    |
| `==`    | **Equal to**, binary                 |
| `!=`    | **Not equal to**, binary             |


## Variables ##

SCL allows to declare variables. Variable name can start only with letter and can contain letters, 
digits and underscore. 
Language keywords cannot be used as variable name.

***Syntax***
```javascript
<varname> := <arithmetic expression> ;
```
All variables are global.  
  
***Examples***
```javascript
my_var:=15;
```
```javascript 
myVar1 := 17 - 8 + my_var;
```

## Expressions ##

Various arithmetic and relational expressions are supported.

### Arithmetic expressions

Arithmetic expression can be used in assignment and result statements. 
It also can used as a part of relational expression.
Expression can contain _numbers_, _variables_, _arithmetic operators_ and _parentheses_.

***Example*** 
```javascript
a := -1+2; //1
b := 2+2*4; //10
c := 34/2-8*4/2+9 + 9*b;  //100
d := 900+(-(55-155)); //1000
e := d*9 + c*9 +(10 - (3 + 2)) *4 - (6+1) + 87; //10000
``` 

### Relational expressions

Relational expression can be used only as condition in control flow statements.
Relational expression contains two arithmetic expressions as operands and relational operator.
See Control Flow Statements section for examples with relational expressions.


## Control flow statements ##

These language constructions allow you to execute (or not) statements depending on a given condition.
Nested control flow statements of any depth are supported.

### if ###

If condition is true then statements between braces will be executed. 
Otherwise not.
Keyword ***if*** is used for this statement.
 
***Syntax***
```javascript
if (<condition>) { 
  <statements> 
}
```

***Examples*** 
```javascript
if (a < 50) {
  a:=50;
}
```
```javascript
if (((100*a)-25)/8 < 50) {
  a:=50;
  if (b != a+18) {
    b:=a;
  }
}
```

### while ###

The while statement continually executes statements between braces while the condition is true.
Keyword ***while*** is used for this statement. 

***Syntax***
```javascript
while (<condition>) { 
  <statements> 
}
```

***Example***
```javascript
while (counter > 0) {
  sum = sum + counter;
  counter := counter - 1;
}
```


## Parameters ##

Script can declare parameters that must be provided to it. 
Parameter declaration statements must be the first statements in a script. 
Parameters act like usual variables.
Keyword ***parameter*** is used for this statement. 

***Syntax***
~~~~
parameter <parameter name>;
~~~~
***Example***
```javascript
parameter a;
parameter b;
sum:= a + b;
```

## Returning result ##

SCL script can return calculation result. 
Keyword ***result*** is used for this statement.

***Syntax***
~~~~
result <arithmetic expression> ;
~~~~

***Examples***
```javascript
result 25;
```
```javascript
result a*14 - 56;
```


## Raising error ##

SCl script can raise an error. 
Keyword ***error*** is used for this statement.
 
 ***Syntax***
```
 error '<error message>' ;
```

***Examples***
```javascript
error 'Unexpected error occured!';
```
```javascript
if (number < 0) {
  error 'Negative numbers are not allowed.';
}
```

## Running SCL scripts ##

There are two ways to launch SCL script: _command line_ and _Java API_. 

All latest builds can be downloaded [here](https://bitbucket.org/eval-p/simple-calculation-language/downloads/).

###Command line###

**scl-<version>.jar** is an executable jar to work with SCL from command line. 
It contains all necessary dependencies inside. 
This way of running SCL scripts requires only _Java Virtual Machine (8+)_ being installed on the computer. 
No additional dependencies needed.

Command line usage:
```
java -jar scl-1.1.jar <script-name> [<script-args>]
```

***Example***
```
java -jar ~/scl_interpreter/scl-1.1.jar ~/my_scl_scripts/fibonacci_number.scl 7
```
If fibonacci_number.scl and scl-1.1.jar are in current directory command can be simplified to:
```
java -jar scl-1.1.jar fibonacci_number.scl 7
```
The following output will be produced:
```
Calculation successful.
Calculation result: 13
```


###Java API###

**scl-api-<version>.jar** contains classes to work with SCL in Java.
Java Virtual Machine (8+) must be installed on the computer to work with SCL.
Also [antlr4-runtime-4.7](https://mvnrepository.com/artifact/org.antlr/antlr4-runtime/4.7) must be in classpath.

Class `org.scl.Scl` contains methods `interpretFromFile(filename, args)` and `interpretFromString(script, args)` to run script from file or string with it's content.
Class `org.scl.Result` contains calculation result.

***Example***
```java 
Result result = Scl.interpretFromFile("/home/scl_user/scl_scripts/fibonacci.scl", 25);
if (result.isSuccessful()) {
  System.out.println("Calculation result: " + result.getValue());
} else {
  System.err.println("Calculation failed: " + result.getErrorMessage());
}
```

----

# SCL examples #

**Calculating absolute value:**
```javascript
//calculates absolute value of parameter a;

parameter a;

if (a < 0) {
  result -a;
}

result a;
```

**Finding greatest common divisor:**
```javascript
//returns greatest common divisor of a and b

parameter a;
parameter b;

while (b != 0) {
  t:=b;

  //calculating remainder of the division
  b:= a - (a/b)*b;

  a:=t;
}

if (a < 0) {
  result -a;
}

result a;
```

**Factorial calculation:**
```javascript
//calculates n!

parameter n;

if (n < 0) {
  error 'Attempt to calculate factorial for negative number.';
}

factorial:=1;

while (n > 0) {
  factorial:=factorial*n;
  n:=n-1;
}

result factorial;
```
